#
# This file is covered by an MIT license, please refer to the LICENSE
# file for more information
#

log_error() {
    local rc=$1
    [[ $rc -eq 0 ]] && return

    shift
    local msg="$*"
    echo "$msg" >&2
    exit $rc;
}

_get_repo_dir() {
    local repo=$1
    echo "${repository[$repo]}"
}

_git_clone() {
    local repo=$1

    local repo_dir="src/$(_get_repo_dir $repo)"
    [[ -d "$repo_dir" ]] && return 0;

    local r=$(_get_git_repo_uri $repo)
    git clone $r "$repo_dir"
}

_get_git_repo_uri() {
    local repo=$1
    local gitrepo=${repository_base[$1]}
    [[ -z $gitrepo ]] && gitrepo=${repository_base[base]}
    [[ -z $gitrepo ]] && log_error 1 "No git repository defined for $repo"

    echo $gitrepo/$(_get_repo_dir $repo).git
}

git_clone() {
    local repo=$1

    ( _git_clone $repo )
    log_error $? "Failed to clone repo $repo"

    echo "Repository $repo ready."
}

init() {
    echo "Initializing metacpan-docker repositories:"
    mkdir -p src
    for repo in "${(@k)repository}"; do
        git_clone $repo
    done

    echo "Ready!";
}

_rewrite_docker_override_version() {
    local dir=$1
    docker_override="$dir"/docker-compose.override.yml
    # Make sure the versions are correct
    [ ! -e $docker_override ] && return;

    version=$(grep "^version:" $dir/docker-compose.yml)
    version_override=$(grep "^version:" $docker_override)
    if [ "$version" != "$version_override" ]
    then
        echo "Setting $docker_override to $version";
        sed -i -e "s/^version:.*/$version/" $docker_override
    fi
}

docker_build() {
    stop_containers
    build_containers
    create_containers
}

stop_containers() {
    echo "Stopping containers"
    docker-compose rm -f -s $@
}

build_containers() {
    local pull=${1:-1}

    if [[ $pull -ne 1 ]]
    then
        docker-compose build $@
        return;
    fi
    docker-compose pull $@
    docker-compose build --pull $@
}

create_containers() {
    local start=${1:-0}

    if [[ $start -eq 1 ]]
    then
        docker-compose up -d $@
    else
        docker-compose up --no-start $@
    fi
}

# vim: ft=zsh
