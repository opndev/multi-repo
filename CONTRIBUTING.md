# Contributing to Multi Repo

## Introduction

Multi Repo is an Open Source project and as such you are free to modify the
source code. Because the project is licensed by an MIT license you are also
free to use this in a closed source environment. We welcome efforts from closed
source companies to submit patches where needed.

## Signing of your commits

In order to validate that you agree with the above statement and the
Developer’s Certificate of Origin you need to sign-off your commits:

```
git commit -s
# or when rebasing
git rebase --signoff
```

You should use your real name, pseudonyms or anonymous contributions are
unfortunately not accepted.

# Developer’s Certificate of Origin 1.1

Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
