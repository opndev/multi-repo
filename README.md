# Welcome to the Multi Repo project

This repository is/creates boilerplate to allow projects to have multiple
repositories for a single project.

This project has stolen the concept from the
[MetaCPAN](https://github.com/metacpan/metacpan-docker) project and generalized
it to have it available for any project that uses multiple repositories.

## Starting a new project

```
git clone git@gitlab.com:opndev/multi-repo.git mynewproject
cd mynewproject
./bin/start "My cool project name" git@gitlab.com/project.git
```

## Project configuration

The project configuration can be found in `etc/multi-project.conf`. Change
these default values to select the correct settings for your various
repositories.

`repository_base` is used to select the correct git repository URI.
`repository` is used to select the correct repository. This name is used in the
`src` directory and is important for docker-compose to know to know which files
it needs to mount.

## multi-repo

The command line tool, written in zsh, to create and maintain a multi
repository project.

## docker-compose.yml

A boilerplate docker-compose.yml for your project, you should change the
mountpoints in order to start your project.

## README.developers.md

A README for developers who want to work with this repository. Change where
needed.

